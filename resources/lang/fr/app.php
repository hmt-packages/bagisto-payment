<?php

return [
    'admin' => [
        'system' => [
            'e-transactions'        => 'E-Transactions',
            'e-transactions-3x'     => 'E-Transactions 3 fois',
            'type'                  => 'Type',
            'delay'                 => 'Retard',
            '3ds'                   => '3D sécurisé',
            '3ds_enabled'           => 'Activer/Désactiver',
            'minimal_amount'        => 'Montant minimal',
            'etransactions_account' => 'Compte E-Transactions',
            'site_number'           => 'Numéro de site',
            'rank_number'           => 'Numéro de rang',
            'login'                 => 'S\'identifier',
            'hmac'                  => 'HMAC',
            'environment'           => 'Environnement',
            'technical_settings'    => 'Paramètres techniques',
            'allowed_ips'           => 'IP autorisées',
            'bank-transfer'         => 'Virement',
            'instructions'          => 'Instructions',
        ],
    ],
];
